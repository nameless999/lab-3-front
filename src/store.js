import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
import router from './router'

Vue.use(Vuex, axios, router);

export default new Vuex.Store({
    state: {
        temperatures: [],
        predictions: []
    },

    mutations: {
        getTemperature(state)
        {
            axios.get('http://localhost:3000/temperatures/getTemperatures')
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response.data)
                    state.temperatures = response.data
                })
                .catch(error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                })
        },
        getPredictions(state)
        {
            axios.get('http://localhost:3000/forecasting/getForecasting')
                .then(response => {
                    // eslint-disable-next-line no-console
                    console.log(response.data)
                    state.predictions = response.data
                })
                .catch(error => {
                    // eslint-disable-next-line no-console
                    console.log(error)
                })
        },
    }
})
